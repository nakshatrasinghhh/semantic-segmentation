FROM python:3.7.9-slim

WORKDIR /app

COPY . /app

RUN pip3 install -r requirements.txt

EXPOSE 4000

ENTRYPOINT [ "streamlit" ]

CMD ["run", "app.py"]
 